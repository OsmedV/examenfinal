// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC1Yt8tc1DntcUikQk3_Q8-jBRTQORIhpk",
    authDomain: "proyectoreservaciones.firebaseapp.com",
    databaseURL: "https://proyectoreservaciones.firebaseio.com",
    projectId: "proyectoreservaciones",
    storageBucket: "proyectoreservaciones.appspot.com",
    messagingSenderId: "767499157476",
    appId: "1:767499157476:web:ccffc73a597d40e52651f1",
    measurementId: "G-LBM6KN9L8K"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.