import { Component, OnInit } from "@angular/core";
import { propiedades } from "../propiedades.model";
import { PropiedadesService } from "../propiedades.service";
import { Location } from "@angular/common";
import { AlertController } from "@ionic/angular";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: "app-agregar",
  templateUrl: "./agregar.page.html",
  styleUrls: ["./agregar.page.scss"],
})
export class AgregarPage implements OnInit {
  form: FormGroup;
  constructor(
    private fb: FormBuilder,
    private servicio: PropiedadesService,
    private _location: Location,
    private alert: AlertController,
    private router: Router,
  ) {}

  ngOnInit() {
    this.createForm();
  }

  onClick() {
    //aca recibo todos los servicios del formulario
    this.servicio.createPropiedad(
      this.form.value.nombre,
      this.form.value.descripcion,
      { lat: "51.531982", long: "-0.17725" },
      {
        ac: this.form.value.ac=(!this.form.value.ac)?false:true,
        cocina: this.form.value.cocina=(!this.form.value.cocina)?false:true,
        mascotas: this.form.value.mascotas=(!this.form.value.mascotas)?false:true,
        piscina: this.form.value.piscina=(!this.form.value.piscina)?false:true,
        wifi: this.form.value.wifi=(!this.form.value.wifi)?false:true,
      },
      this.form.value.precio,
      this.form.value.imagen
    );
    console.log(localStorage.getItem('usuario'));
    console.log(this.form);
    this.router.navigate(["/home"]);
  }

  createForm() {
    this.form = this.fb.group({
      nombre: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      descripcion: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      ubicacion: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      //map comodidades below
      ac: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      cocina: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      mascotas: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      piscina: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      wifi: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      // end map
      precio: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      imagen: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
    });
  }
}
