import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ByUserPage } from './byUser.page';

const routes: Routes = [
  {
    path: '',
    component: ByUserPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ByUserPageRoutingModule {}
