//acá se supone que van las interfaces
//interface y todos los elementos que yo voy a tener
//en el modelo yo establesco la interface
// SERVICIOS clase metodos, elementos de una clase
export interface propiedades {
  id: number;
  nombre: string;
  descripcion: string;
  ubicacion: {
    lat: string;
    long: string;
  };
  comodidades: {
    ac: boolean;
    cocina: boolean;
    mascotas: boolean;
    piscina: boolean;
    wifi: boolean;
  };
  precio: number;
  imagen: string;
}
//clase 7 o 8 minuto 20:31 servicios

//este export es como un create
export class propiedades implements propiedades {
  constructor(
    public nombre: string,
    public descripcion: string,
    public ubicacion: {
      lat: string;
      long: string;
    },
    public comodidades: {
      ac: boolean;
      cocina: boolean;
      mascotas: boolean;
      piscina: boolean;
      wifi: boolean;
    },
    public precio: number,
    public imagen: string
  ) {}
}
