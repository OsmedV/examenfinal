import { Component, OnInit } from "@angular/core";
import { propiedades } from "./propiedades.model";
import { PropiedadesService} from "./propiedades.service";
import { Location } from "@angular/common";
import { Router, Params } from "@angular/router";
import { AngularFirestore } from '@angular/fire/firestore';
import { first } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: "app-propiedades",
  templateUrl: "./propiedades.page.html",
  styleUrls: ["./propiedades.page.scss"],
})
export class PropiedadesPage implements OnInit {

  public items: any[];

  constructor(
    private servicio: PropiedadesService,
    private _location: Location,
    private router: Router,
    private firestore: AngularFirestore
  ) {}

  async ngOnInit() {
    this.items = await this.initializeItems();
  }

  async initializeItems(): Promise<any> {
    const lista = await this.firestore.collection('propiedades')
    .snapshotChanges().pipe(first()).toPromise();
    return lista;
  }
  
  viewDetails(item){
    this.router.navigate(['/propiedades/detalle/'+ item.payload.doc.id]);
  }

  async filterList(evt) {
    this.items = await this.initializeItems();
    const searchTerm = evt.srcElement.value;
  
    if (!searchTerm) {
      return;
    }
  
    this.items = this.items.filter(propiedad => {
      if (propiedad.payload.doc.data().nombre && searchTerm) {
        return (propiedad.payload.doc.data().nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 || propiedad.payload.doc.data().descripcion.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1);
      }
    });
  }
  
  goEdit(item){
    this.router.navigate(['/propiedades/editar/'+ item.payload.doc.id]);
  }

}
