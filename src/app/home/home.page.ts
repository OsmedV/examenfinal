import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { MenuController } from "@ionic/angular";
import { PropiedadesService } from "../propiedades/propiedades.service";
import { AngularFirestore } from '@angular/fire/firestore';
import { first } from 'rxjs/operators';
import { AuthService } from '../core/auth.service';
import { Location } from '@angular/common';

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {

  public items = [];
  
  constructor(
    private route: Router, 
    private menuController: MenuController,
    private servicio: PropiedadesService,
    private firestore: AngularFirestore,
    private location : Location,
    public authService: AuthService,
    ) {}

  ngOnInit() {
    this.items = this.servicio.obtenerPropiedades(5);
  }
  currentUser = localStorage.getItem('usuario');

  goPropiedades() {
    this.route.navigate(["/propiedades"]);
  }
  goLogin() {
    this.route.navigate(["/login"]);
  }

  //copy paste de tryLogout
  gologout(){
    this.authService.doLogout()
    .then((res) => {
      this.location.back();
    }, (error) => {
      console.log("Logout error", error);
    });
  }

  goUser() {
    if (this.currentUser) {
      this.route.navigate(["/user"]);
    } else {
      this.goLogin();
    }
  }

  async openMenu() {
    await this.menuController.open();
  }

}
